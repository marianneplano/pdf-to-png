function make_pngs(){

	PAGES=("$@")
	PDF=$1

	PAGES_DIR="${PDF%.*}_pages"
	THUMB_DIR="${PAGES_DIR}/thumbnails/${2}"

	mkdir -p $PAGES_DIR
	mkdir -p $THUMB_DIR

	for i in ${!PAGES[@]}; do

		if (( $i > 1 )); then

			PAGE="${PAGES[$i]}"
			PAGE_QUERY=$((PAGE - 1)) 
			PAGE_ODD=$((PAGE_QUERY - 1))
			FINAL_PAGE_ONE=$(printf "%03d" $PAGE_QUERY) 
			FINAL_PAGE_TWO=$(printf "%03d" $PAGE)
			FINAL="${PAGES_DIR}/page_${FINAL_PAGE_ONE}-${FINAL_PAGE_TWO}.png"
			THUMB="${THUMB_DIR}/page_${FINAL_PAGE_ONE}-${FINAL_PAGE_TWO}.png"

			if [ ! -f $FINAL ]; then

				FILE="png-even_${PAGE}.png"
				ODD_FILE="png-odd_${PAGE}.png"

				convert -density 150 ${PDF}[${PAGE_QUERY}]  -quality 100 -sharpen 0x1.0 -resize 100% ${ODD_FILE}
				convert -density 150 ${PDF}[${PAGE_ODD}]  -quality 100 -sharpen 0x1.0  -resize 100% ${FILE}

				H=$(identify -format '%h' ${FILE})
				W=$(identify -format '%w' ${FILE})

				GRADIENT="even-gradient_${PAGE}.png"
				ODD_GRADIENT="odd-gradient_${PAGE}.png"

				magick -density 150 -size ${H}x700 -define gradient:vector=0,-200,0,40  gradient:darkgrey-none -colorspace RGB -colorspace gray -background white -rotate 270 $ODD_GRADIENT
				magick -density 150 -size ${H}x700 -define gradient:vector=0,-200,0,40  gradient:darkgrey-none -colorspace RGB -colorspace gray -background white -rotate 90 $GRADIENT

				POS=$((W - 700))

				EVEN_PAGE="even-page_${PAGE}.png"
				ODD_PAGE="odd-page_${PAGE_QUERY}.png"

				CIRCLE_SIZE=30

				convert $FILE $GRADIENT -geometry +$POS -composite -background white -alpha remove -alpha off $EVEN_PAGE
				convert $EVEN_PAGE   \
			     \( +clone  -alpha extract \
			        -draw "fill black polygon $W,0 $W,$CIRCLE_SIZE $((W - CIRCLE_SIZE)),0 fill white circle $((W - CIRCLE_SIZE)),$CIRCLE_SIZE $((W - CIRCLE_SIZE)),0" \
			        \( +clone -flip \) -compose Multiply -composite \
			     \) -alpha off -compose CopyOpacity -composite $EVEN_PAGE
				convert $ODD_FILE $ODD_GRADIENT -composite -background white -alpha remove -alpha off $ODD_PAGE
				convert $ODD_PAGE   \
			     \( +clone  -alpha extract \
			        -draw "fill black polygon 0,0 0,$CIRCLE_SIZE 30,0 fill white circle $CIRCLE_SIZE,$CIRCLE_SIZE $CIRCLE_SIZE,0" \
			        \( +clone -flip \) -compose Multiply -composite \
			     \) -alpha off -compose CopyOpacity -composite  $ODD_PAGE

				convert  +append $EVEN_PAGE $ODD_PAGE $FINAL

				rm $ODD_FILE $FILE $GRADIENT $ODD_GRADIENT $EVEN_PAGE $ODD_PAGE

			else

				echo "File $FINAL already exists."

			fi

			if [ ! -f $THUMB ]; then

				convert $FINAL -resize $2 $THUMB

			fi
		fi
			
	 done	
}

make_pngs "file.pdf" 850 5 7 9 29 145



